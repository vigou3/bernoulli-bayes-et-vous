### Emacs: -*- coding: utf-8; fill-column: 62; comment-column: 27; -*-
##
## Copyright (C) 2018-2020 Vincent Goulet
##
## Ce fichier fait partie du projet «Bernoulli, Bayes et votre
## dossier de conduite»
## http://gitlab.com/vigou3/bernoulli-bayes-et-vous
##
## Cette création est mise à disposition selon le contrat
## Attribution-Partage dans les mêmes conditions 4.0
## International de Creative Commons.
## http://creativecommons.org/licenses/by-sa/4.0/

library(shiny)

###
### bayes(x, prob, urns = 1/length(prob))
###
##
## Calcule les probabilités à posteriori d'une expérience «urne
## d'urnes».
##
## Arguments
##
## x:    liste de vecteurs de résultats (0 ou 1)
## prob: vecteur de probabilités de succès (1) dans chaque urne
## urns: vecteur de probabilités à priori pour chaque urne
##
## Résultat
##
## Graphique à bande représentant les probabilités à posteriori pour
## chaque urne et liste (invisible) des probabilités à posteriori.
bayes <- function(x, prob, urns = 1/length(prob))
{
    if (missing(x))
        x <- sapply(rep.int(0, length(prob)), numeric)

    nurns <- length(urns)
    n <- length(x[[1]])

    nums <- lapply(x, function(x) {y <- sum(x); prob^y * (1 - prob)^(n - y) * urns})

    res <- lapply(nums, function(x) x/sum(x))

    premium <- lapply(res, function(x) drop(crossprod(prob, x)))

    op <- par(mfcol = c(1, nurns), mar = c(5, 4, 2, 2))
    colors <- c("Orchid2", "PaleGreen2", "LightBlue2", "black")
    for (i in seq_len(nurns))
        barplot(res[[i]], ylim = c(0, 1), col = colors[i],
                main = paste("Moyenne:", format(premium[[i]], digits = 2)))
    par(op)

    invisible(res)
}

###
### App
###

## Interface
ui <- fluidPage(

    ## Titre
    title = "Bayes et la protection contre l'infortune",

    fluidRow(
        column(5,
               h4("Niveaux de risque"),
               column(4,
                      numericInput("probL", "Bon", value = 0.3,
                                   min = 0, max = 1, step = 0.1)),
               column(4,
                      numericInput("probM", "Moyen", value = 0.5,
                                   min = 0, max = 1, step = 0.1)),
               column(4,
                      numericInput("probH", "Mauvais", value = 0.8,
                                   min = 0, max = 1, step = 0.1))),
        column(5, offset = 1,
               h4("Opinion à priori"),
               column(4,
                      textInput("urnA", "A", "1/3")),
               column(4,
                      textInput("urnB", "B", "1/3")),
               column(4,
                      textInput("urnC", "C", "1/3"))),
        column(1)
    ),

    hr(),

    h4("Résultats"),
    fluidRow(
        column(1,
               tags$p(HTML("<strong>Équipe A</strong>"))),
        column(1,
               radioButtons("resA.1", "Pige 1",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.2", "Pige 2",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.3", "Pige 3",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.4", "Pige 4",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.5", "Pige 5",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.6", "Pige 6",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.7", "Pige 7",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.8", "Pige 8",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resA.9", "Pige 9",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(2,
               radioButtons("resA.10", "Pige 10",
                            c("R" = 1, "N" = 0),
                            selected = character(0)))
    ),
    fluidRow(
        column(1,
               tags$p(HTML("<strong>Équipe B</strong>"))),
        column(1,
               radioButtons("resB.1", "Pige 1",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.2", "Pige 2",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.3", "Pige 3",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.4", "Pige 4",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.5", "Pige 5",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.6", "Pige 6",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.7", "Pige 7",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.8", "Pige 8",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.9", "Pige 9",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resB.10", "Pige 10",
                            c("R" = 1, "N" = 0),
                            selected = character(0)))
    ),
    fluidRow(
        column(1,
               tags$p(HTML("<strong>Équipe C</strong>"))),
        column(1,
               radioButtons("resC.1", "Pige 1",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.2", "Pige 2",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.3", "Pige 3",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.4", "Pige 4",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.5", "Pige 5",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.6", "Pige 6",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.7", "Pige 7",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.8", "Pige 8",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(1,
               radioButtons("resC.9", "Pige 9",
                            c("R" = 1, "N" = 0),
                            selected = character(0))),
        column(2,
               radioButtons("resC.10", "Pige 10",
                            c("R" = 1, "N" = 0),
                            selected = character(0)))
    ),

    actionButton("goButton", "Calculer"),

    hr(),

    fluidRow(column(8, plotOutput("zePlot"), offset = 2))
)

## Serveur
server <- function(input, output) {

    ## Vecteur des niveaux de risque
    probs <- reactive({
        c(Bon     = input$probL,
          Moyen   = input$probM,
          Mauvais = input$probH)
    })

    ## Vecteur des probabilités à priori
    urns <- reactive({
        c(A = eval(parse(text = input$urnA)),
          B = eval(parse(text = input$urnB)),
          C = eval(parse(text = input$urnC)))
    })

    ## Liste des résultats
    data <- reactive({
        list(A = c(as.numeric(input$resA.1), as.numeric(input$resA.2), as.numeric(input$resA.3),
                   as.numeric(input$resA.4), as.numeric(input$resA.5), as.numeric(input$resA.6),
                   as.numeric(input$resA.7), as.numeric(input$resA.8), as.numeric(input$resA.9),
                   as.numeric(input$resA.10)),
             B = c(as.numeric(input$resB.1), as.numeric(input$resB.2), as.numeric(input$resB.3),
                   as.numeric(input$resB.4), as.numeric(input$resB.5), as.numeric(input$resB.6),
                   as.numeric(input$resB.7), as.numeric(input$resB.8), as.numeric(input$resB.9),
                   as.numeric(input$resB.10)),
             C = c(as.numeric(input$resC.1), as.numeric(input$resC.2), as.numeric(input$resC.3),
                   as.numeric(input$resC.4), as.numeric(input$resC.5), as.numeric(input$resC.6),
                   as.numeric(input$resC.7), as.numeric(input$resC.8), as.numeric(input$resC.9),
                   as.numeric(input$resC.10)))
    })

    ## Création du graphique
    output$zePlot <- renderPlot({
        input$goButton
        isolate(bayes(data(), probs(), urns()))
    })
}

## Application
shinyApp(ui = ui, server = server)
