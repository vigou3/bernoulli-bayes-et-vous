# Bernoulli, Bayes et votre dossier de conduite

Diapositives d'une conférence prononcée dans divers contextes et pour
des auditoires divers.

## Résumé

Les actuaires sont des experts de l'évaluation et de la tarification des risques en assurance. Prenons l'exemple de l'assurance automobile, celle la plus familière dès lors que l'on dispose d'un permis de conduire. Les actuaires doivent déterminer le montant moyen des primes au sein d'un groupe afin d'assurer la viabilité du système à long terme, mais également la répartition la plus équitable de ces primes parmi les assurés en fonction de leur dossier de conduite.

Dans leur arsenal, les actuaires disposent de grands principes de probabilité et de statistique. L'assurance repose tout entière sur la Loi des grands nombres édictée par Jacques Bernoulli, alors que l'évaluation des risques en fonction de l'expérience se base sur le Théorème de Bayes. Nous illustrerons ces principes à l'aide de jeux et de simulations.

La présentation imposera également une réflexion sur la nature d'une probabilité. Entre la probabilité de gagner à pile ou face et la probabilité d'averse au cours de la journée, y aurait-il une différence fondamentale?

## Durée

30-45 minutes
