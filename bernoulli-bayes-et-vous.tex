%%% Copyright (C) 2018-2023 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include et \input ci-dessous font
%%% partie du projet «Bernoulli, Bayes et votre dossier de conduite»
%%% http://gitlab.com/vigou3/bernoulli-bayes-et-vous
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% http://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{animate}                   % gif animé
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% ==================
  %%  Publication info
  %% ==================
  \title{Bernoulli, Bayes et la protection contre l'infortune}
  \author{Vincent Goulet}
  \renewcommand{\year}{2023}
  \renewcommand{\month}{02}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/bernoulli-bayes-et-vous/}

  %% ===============================
  %%  Look and feel of the document
  %% ===============================

  %% Beamer theme
  \usetheme{metropolis}

  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}

  %% Additionnal colors
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \colorlet{alert}{mLightBrown} % alias for Metropolis color
  \colorlet{dark}{mDarkTeal}    % alias for Metropolis color

  %% Hyperlinks
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Bernoulli, Bayes et votre dossier de conduite},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% ===============================
  %%  New commands and environments
  %% ===============================

  %% Renvoi vers GitHub sur la page de copyright
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Lengths used to compose front and rear covers.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}     \newlength{\imagewidth}
  \newlength{\logoheight}
  \newlength{\gapwidth}

%  \includeonly{licence}

\begin{document}

%% frontmatter
\input{couverture-avant}
\input{notices}

%% mainmatter
\begin{frame}[standout]
  Qu'est-ce qu'une probabilité?
\end{frame}

\begin{frame}[plain]
  Vous jouez à pile ou face avec une pièce équilibrée.

  Les tirs sont indépendants.

  Après quatre tirs, vous avez obtenu les
  résultats suivants: F, F, F, F.

  Quelle est la probabilité que le prochain résultat soit P?
  \bigskip

  \textcolor{cyan}{   \rule[-2.2mm]{7mm}{7mm}}\quad $\frac{1}{32}$
  \hspace{10mm}
  \textcolor{magenta}{\rule[-2.2mm]{7mm}{7mm}}\quad $\frac{1}{2}$
  \hspace{10mm}
  \textcolor{orange}{ \rule[-2.2mm]{7mm}{7mm}}\quad $0$
  \hspace{10mm}
  \textcolor{black}{  \rule[-2.2mm]{7mm}{7mm}}\quad $1$
\end{frame}

\begin{frame}[plain]
  Vous gagnez \nombre{1000000}~\$ si vous prédisez correctement le
  résultat du 5{\ieme} tir.

  Quelle est votre prédiction?
  \bigskip

  \textcolor{cyan}{   \rule[-2.2mm]{7mm}{7mm}}\quad P
  \hspace{10mm}
  \textcolor{magenta}{\rule[-2.2mm]{7mm}{7mm}}\quad F
\end{frame}

\begin{frame}[plain]
  Après \nombre{100 000} tirs indépendants à pile ou face, vous avez
  obtenu les résultats suivants: \nombre{74873} F, \nombre{25127} P.

  Quelle est la probabilité que le prochain résultat soit P?
  \bigskip

  \textcolor{cyan}{   \rule[-2.2mm]{7mm}{7mm}}\quad $\frac{1}{4}$
  \hspace{10mm}
  \textcolor{magenta}{\rule[-2.2mm]{7mm}{7mm}}\quad $\frac{1}{2}$
  \hspace{10mm}
  \textcolor{orange}{ \rule[-2.2mm]{7mm}{7mm}}\quad $0$
  \hspace{10mm}
  \textcolor{black}{  \rule[-2.2mm]{7mm}{7mm}}\quad $1$
\end{frame}

\begin{frame}[plain]
  Le nombre de piles et le nombre de faces devraient tendre à
  s'équilibrer.

  En vertu de quel principe?
  \bigskip

  \textcolor{cyan}{   \rule[-2.2mm]{7mm}{7mm}}\quad
  Théorème de Bernoulli

  \textcolor{magenta}{\rule[-2.2mm]{7mm}{7mm}}\quad
  Loi de la moyenne

  \textcolor{orange}{ \rule[-2.2mm]{7mm}{7mm}}\quad
  Loi des grands nombres

  \textcolor{black}{  \rule[-2.2mm]{7mm}{7mm}}\quad
  Théorème de Pythagore
\end{frame}

\begin{frame}
  \frametitle{Loi des grands nombres}

  \begin{minipage}{0.48\linewidth}
    \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{images/Jakob_Bernoulli} \\
    \small Jacques (ou Jakob) Bernoulli (1654--1705)
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\linewidth}
    Loi fondamentale de l'assurance
    \begin{equation*}
      \bar{X}_n \rightarrow \mu \text{ quand } n \rightarrow \infty
    \end{equation*}
  \end{minipage}
\end{frame}

%% App "shiny/loi-grands-nombres.R"

\begin{frame}[standout]
  \begin{textblock*}{0.7\paperwidth}(60mm,10mm)
    \pgfsetfillopacity{0.5}
    \includegraphics[width=\linewidth,keepaspectratio]{images/959px-Font_Awesome_5_solid_car-crash}
    \pgfsetfillopacity{1.0}
  \end{textblock*}
  \begin{textblock*}{\paperwidth}(0mm,40mm)
    Conception d'un système d'assurance simple
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Théorème de Bayes}

  \begin{minipage}{0.48\linewidth}
    \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{images/Thomas_Bayes} \\
    \small Thomas Bayes (c. 1701--1761)
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\linewidth}
    Solution au problème de \alert{probabilité \\ inverse}
    \begin{equation*}
      \Pr[B|A] = \frac{\Pr[A|B] \Pr[B]}{\Pr[A]}
    \end{equation*}
  \end{minipage}
\end{frame}

%% App "shiny/bayes.R"

\begin{frame}[standout]
  \begin{textblock*}{0.7\paperwidth}(70mm,0mm)
    \pgfsetfillopacity{0.5}
    \includegraphics[width=\linewidth,keepaspectratio]{images/Tool_box_icon-01}
    \pgfsetfillopacity{1.0}
  \end{textblock*}
  \begin{textblock*}{\paperwidth}(0mm,40mm)
    \bfseries%
    actuaire /aktɥɛʁ/ \\
    \mdseries%
    Ingénieur de l'assurance
  \end{textblock*}
\end{frame}

% \begin{frame}[plain]
%   \begin{textblock*}{\paperwidth}(0mm,0mm)
%     \includegraphics[width=\paperwidth,keepaspectratio]{images/2018_offre-conference_PPT_Cover}
%   \end{textblock*}
% \end{frame}

\begin{frame}[plain]
  \centering

  Merci de votre participation!
\end{frame}

%% backmatter
\input{colophon}

\end{document}

%%% Local Variables:
%%% mode: noweb
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
